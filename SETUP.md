## Useful commands

### For dev setup

Make sure your go binaries get correctly accessible system-wide. Typically, in order to build the self-contained listmonk binary you need the "stuffbin" go binary written by knadh, and downloaded if not present.

By default, go binaries are in `/usr/lib/go/bin` and `$HOME/go/bin`.

Either add those folders to you PATH with

```bash
export PATH=/usr/lib/go/bin:$HOME/go/bin:$PATH
```

or create symlinks to the specific binaries in a folder from you PATH, like:

```bash
sudo ln -s ~/go/bin/stuffbin /usr/bin/stuffbin
```

### Global (dev & prod)

If you don't have any dedicated postgresql database, you'll need to create one.

First login using

```bash
psql -U username -h localhost
```

and then create the database with

```sql
create database database_name;
```

Nota: of course, the `database_name` must match the one you've set in your `config.toml` file.